<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Metadata -->
    <meta charset="UTF-8">
    <title>Agent</title>
    <link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16">
    <link rel="stylesheet" href="https://192.168.4.61:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.4.61:8443/assistserver/sdk/web/shared/css/shared-window.css">

    <!-- Internal dependencies -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>

   <!-- header -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CafeX</a>
            </div>

            <ul class="nav navbar-nav pull-right">
                <li><a href="about.html">About</a></li>
                <li><a href="about.html">Product</a></li>
                <li><a href="about.html">Contact</a></li>
                <!--             <button id="go" class="btn btn-info">Generate Short-Code</button> -->
                <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal" id="go">Generate Short-Code</button>
                <!--  <h2 id="short-code"></h2> -->
            </ul>

            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">

                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                        </div>
                        <div class="modal-body">
                            <p>Generated Short-code: <span id="short-code"></span></p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </nav>
<!-- header -->

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <!-- The button to end an Assist Session will only be visible while assist is active -->
        <button style="display: none;" id="end">End support</button>
    </div>

    <!-- -->
    <div class="container">
    <h3>Agent End</h3>
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <div class="row">

            <!-- This div is for local and remote preview -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-right preview">
                <div id="local" class="draggable">
                    <h4>Local Preview</h4>
                </div>
                <!-- Remote Preview -->
                <div id="remote" class="draggable">
                    <h4>Remote Preview</h4>
                </div>
                <!-- Request screen share -->
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                    <button id="requestShare" class="btn btn-primary">Request Screen Share</button>
                </div>
            </div>

        </div>
    </div>
    </div>

</body>
<!-- Required libraries and stylesheets for Assist SDK -->

<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>
<!-- js required for voice and video -->
<script src="https://192.168.4.61:8443/gateway/adapter.js"></script>
<script src="https://192.168.4.61:8443/gateway/csdk-sdk.js"></script>
<script src="js/assist-console-callmanager.js"></script>

<!-- js required only for co-browse only -->
<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="https://192.168.4.61:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>
<!-- End! -->

<!-- -->
<script src="https://192.168.4.61:8443/assistserver/sdk/web/consumer/assist.js"></script>
<!-- Load jQuery and Bootstrap  -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>

<script>
    <?php
            require_once('Provision.php');

            $Provision = new Provision;
            $token = $Provision->provisionAgent('agent1', '192.168.4.61', '.*');

            // Decode the session id from the provisioning token
            $sessionId = json_decode($token)->sessionid;
            ?>

    var sessionId = '<?= $sessionId; ?>';
    console.log('Session ID: ' + sessionId);
</script>
<!-- main js file -->
<script src="js/script.js"></script>
<!-- JS librabry for drag functionality -->
<script src="https://code.interactjs.io/v1.2.6/interact.js"></script>
<script src="js/drag.js" type="text/javascript"></script>


<!--script for generating short-code -->
<script>
    // cache the UI elements that we will be interacting with
    var $endButton = $('#end');
    var $codeDisplay = $('#short-code');

    // when the support button is clicked...
    $('#go').click(function(event) {
        event.preventDefault();

        // Retrieve a short code
        $.ajax('https://192.168.4.61:8443/assistserver/shortcode/create', {
                method: 'put',
                dataType: 'json'
            })
            .done(function(response) {

                // Retrieve session configuration (session token, CID etc.)
                $.get('https://192.168.4.61:8443/assistserver/shortcode/consumer', {
                    appkey: response.shortCode
                }, function(config) {

                    // translate the config retrieved from the server, into
                    // parameters expected by AssistSDK
                    var params = {
                        cobrowseOnly: true,
                        correlationId: config.cid,
                        scaleFactor: config.scaleFactor,
                        sessionToken: config['session-token']
                    };

                    // start the support session
                    AssistSDK.startSupport(params);

                    // display the code for the consumer to read to the agent
                    $codeDisplay.text(response.shortCode);
                }, 'json');
            });
    });

    // configure Assist to automatically approve share requests
    AssistSDK.onScreenshareRequest = function() {
        return true;
    };

    // the $endSupport button will be hidden
    $endButton.hide();

    // when screenshare is active, toggle the visibility of the 'end' button
    AssistSDK.onInSupport = function() {
        $endButton.show();
    };

    AssistSDK.onEndSupport = function() {
        $endButton.hide();
        $codeDisplay.text('');
    };

    $endButton.click(function() {
        AssistSDK.endSupport();
        // CallManager.endCall();
    });
</script>

</html>
