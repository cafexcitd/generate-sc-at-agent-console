<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Metadata -->
    <meta charset="UTF-8">
    <title>Consumer</title>
    <link rel="icon" type="image/png" href="img/favicon-16x16.png" sizes="16x16">
    <link rel="stylesheet" href="https://192.168.4.61:8443/assistserver/sdk/web/agent/css/assist-console.css">
    <link rel="stylesheet" href="https://192.168.4.61:8443/assistserver/sdk/web/shared/css/shared-window.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">

    <!-- Internal dependencies -->
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<body>
<!-- header -->
    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <a class="navbar-brand" href="#">CafeX</a>
            </div>

            <ul class="nav navbar-nav pull-right">
                <li><a href="about.html">About</a></li>
                <li><a href="about.html">Product</a></li>
                <li><a href="about.html">Contact</a></li>
                <!-- Trigger the modal with a button -->
                <button type="button" class="btn btn-info btn-lg" id="shortCodeBox">Assist</button>
            </ul>

            <div class="getConnected">
            <form class="assist-session">
                 <input type="text" class="inp shortCode" placeholder="Enter a Short Code">
                 <button class="btn btn-primary" id="cobrowseOnly">Go</button>
            </form>
            </div>

        </div>
    </nav>
    <!-- end of header -->

<div class="container">
<h3>Consumer End</h3>
    <!-- -->
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
        <div class="row">

        <!-- Agent tools -->
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 pull-left leftPanel">
                <div class="agentTools">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="row">
                            <h4>Document Push</h4>
                            <form class="push">
                                <input type="text" class="url form-control" placeholder="URL">
                                <input type="radio" name="type" value="document" checked>Document
                                <input type="radio" name="type" value="content">Content
                                <input type="radio" name="type" value="link">Link
                                <button type="submit" class="btn btn-primary">Push</button>
                            </form>
                            <br />
                            <button id="closeSharedDoc" class="btn btn-primary">Close doc</button>
                        </div>
                    </div>
                    <hr>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left">
                        <div class="row">
                            <h4>Mode</h4>
                            <form class="mode">
                                <input type="radio" name="mode" value="control" checked>Control
                                <input type="radio" name="mode" value="draw">Draw
                                <input type="radio" name="mode" value="spotlight">Spotlight
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <!-- End of Agent tools -->

            <!-- Screen sharing -->
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 pull-left resizeWidth">
                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 pull-left shareResize">
                    <div class="row">
                        <div id="share">
                            <h4>Screen Share</h4>
                        </div>
                        <button class="btn btn-info" id="disconnect">Disconnect</button>
                    </div>
                </div>
            </div>
            <!-- Screen -->

        </div>
    </div>


</div>
</body>
<!-- Required libraries and stylesheets for Assist SDK -->
<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/thirdparty/i18next-1.7.4.min.js"></script>

<!-- js required only for co-browse only -->
<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/assist-aed.js"></script>
<script src="https://192.168.4.61:8443/assistserver/sdk/web/shared/js/shared-windows.js"></script>
<script src="https://192.168.4.61:8443/assistserver/sdk/web/agent/js/assist-console.js"></script>
<!-- End! -->

<script src="https://192.168.4.61:8443/assistserver/sdk/web/consumer/assist.js"></script>

<!-- Load jQuery and Bootstrap  -->
<script src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<!-- <script src="tabs.js"></script>   -->
<script>
    <?php
            require_once('Provision.php');

            $Provision = new Provision;
            $token = $Provision->provisionAgent('Agent1', '192.168.4.61', '.*');
            // $token = provision('priti', 'abc');

            // Decode the session id from the provisioning token
            $sessionId = json_decode($token)->sessionid;
            ?>

    var sessionId = '<?= $sessionId; ?>';
    console.log('Session ID: ' + sessionId);
</script>
<!-- main js file -->
<script src="js/consumer-script.js"></script>

</html>
