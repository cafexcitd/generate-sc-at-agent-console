/**
 * Responsible for audio/video calling along with co-browsing
 * -and- co-browsing-only, sessions.
 */
var AgentModule = function(sessionId) {
    'use strict';

    var remote, local, sharedDocId, type,
        // jQuery elements for easier event bindings
        $push, $share, $closeSharedDoc,
        $assistedSession, $mode, $clear, $requestShare,
        $annotationControl, $disconnect;

    // Functions to execute tasks
    cacheDom();
    setClickHandlers();
    bindAgentSdkCallbacks();
    linkUi();



    function cacheDom() {

        $share = $('#share')[0];       
        $requestShare = $('#requestShare');
        $push = $('.push');
        $closeSharedDoc = $('#closeSharedDoc');
        $assistedSession = $('.assist-session');
        $mode = $('.mode');
        $annotationControl = $('.annotation-control');
        $clear = $('.clear');
        $disconnect = $('#disconnect');
        
    }

    // Add click handlers that determine what happens when the Agent clicks the relevant button
    function setClickHandlers() {
        $('.shortCode').keyup(function() {
            var $this = $(this);

            if ($this.val() != '' && ($this.val() % 1 === 0)) {
                $('.shortcode').not(this).attr('disabled', 'disabled');
                $('#cobrowseOnly').show();

                $('.shareResize').css({
                    "width": "100%"
                });
            } else {
                $('.shortcode').removeAttr('disabled');
                $('#cobrowseOnly').hide();
                console.log('Please enter numbers only');
            }
    });


        $disconnect.click(function() {
            AssistAgentSDK.endSupport();
            $('.agentTools').hide();
            $('.resizeWidth').css({
                "width": "100%"
            });
        });


        /* script for push document */
        $push.submit(function(e) {
            e.preventDefault();
            var $this = $(this);
            var url = $this.find('.url').val();
            var type = $this.find('[name=type]:checked').val();

            switch (type) {
                case 'content':
                    AssistAgentSDK.pushContent(url, function(completionEvent) {
                        type = completionEvent.type;
                        $('.showCallbacks').text('pushContentCallback. Doc ID: ' + type);
                    });

                    break;
                case 'document':
                    AssistAgentSDK.pushDocument(url, function(completionEvent) {
                        sharedDocId = completionEvent.sharedDocId;
                        $('.showCallbacks').text('pushDocumentCallback. Doc ID: ' + sharedDocId);
                    });

                    break;
                case 'link':
                    AssistAgentSDK.pushLink(url, function() {
                        $('.showCallbacks').text('pushLinkCallback');
                    });

                    break;
            }
        });

        // Document close
        $closeSharedDoc.click(function() {
            AssistAgentSDK.closeDocument(sharedDocId);
            $('.showCallbacks').text('closing remote doc. ID: ' + sharedDocId);
        });


        /* js for short-code to connect Agent */
        $assistedSession.submit(function(e) {
            e.preventDefault();
            // Get the shortcode from the field
            var $this = $(this);
            var shortCode = $this.find('.shortCode').val();

            $.get('http://192.168.4.61:8080/assistserver/shortcode/agent', {
                appkey: shortCode
            }, function(config) {
                var settings = {
                    correlationId: config.cid,
                    url: 'http://192.168.4.61:8080',
                    sessionToken: sessionId
                };

                AssistAgentSDK.startSupport(settings);

                // with the support session started, request sharing of the consumer's screen
                AssistAgentSDK.requestScreenShare();
            }, 'json');


            $disconnect.show();
        });

        /* js for Agent tools */
        $mode.change(function() {
            var $this = $(this);
            var mode = $this.find('[name=mode]:checked').val();

            // Determine which mode to switch on
            switch (mode) {
                case 'control':
                    AssistAgentSDK.controlSelected();
                    break;

                case 'draw':
                    AssistAgentSDK.drawSelected();
                    break;

                case 'spotlight':
                    AssistAgentSDK.spotlightSelected();
                    break;
            }
        });


        $clear.click(function() {
            AssistAgentSDK.clearSelected();
        });

        AssistAgentSDK.setScreenShareActiveCallback(function(active) {
            if (active) {
                $('.agentTools').show();
                $('.resizeWidth').css({
                    "width": "70%"
                });

                $('#disconnect').show();

            } else {
                $('.showCallbacks').text('setScreenShareRejectedCallback');
                // alert();
            }
        });

        AssistAgentSDK.setScreenShareRejectedCallback(function() {
            $('.showCallbacks').text('setScreenShareRejectedCallback');
            $requestShare.disabled = false;
        });

         $('#shortCodeBox').click(function(e){
            $('.getConnected').toggle();
         });

    }

    // Agent SDK's callbacks
    function bindAgentSdkCallbacks() {}

    function linkUi() {
        AssistAgentSDK.setRemoteView(share);
    }


}(sessionId);