# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Generate Short-code at Agent Console and pass Short code to Consumer to get connected with Agent.
* LA 1.40

### How do I get set up? ###

Clone the repo.
Placed file on xampp/wamp/mamp folder
Changed the ip-server with your own ip server
Run it on browser, for Agent- localhost/agent
and for consumer- localhost/consumer

### How to Use this:-

=> Click on Generate short-code button at Agent end, displayed 6 digit short-code
=> Paste generated short code in input box at Consumer end(click on Assist button)
=> Now, Consumer would get connected to Agent.


![agent.png](https://bitbucket.org/repo/nqzLpG/images/2237903345-agent.png)
![consumer.png](https://bitbucket.org/repo/nqzLpG/images/611971802-consumer.png)