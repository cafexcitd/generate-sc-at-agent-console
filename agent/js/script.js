/**
 * Responsible for audio/video calling along with co-browsing
 * -and- co-browsing-only, sessions.
 */
var AgentModule = function(sessionId) {
    'use strict';

    var remote, local;

    // Functions to execute tasks
    cacheDom();
    setClickHandlers();
    bindAgentSdkCallbacks();
    linkUi();

    // Initialise the library and register the Agent to receive calls
    init(sessionId);

    function cacheDom() {
        remote = $('#remote')[0];
        local = $('#local')[0];
    }


    function setClickHandlers() {

        AssistAgentSDK.setScreenShareActiveCallback(function(active) {
            if (active) {
                console.log('Screen share active!');
            } else {
                onsole.log('Screen share not active!');
            }
        });

    }

    // Agent SDK's callbacks
    function bindAgentSdkCallbacks() {

        AssistAgentSDK.setConnectionEstablishedCallback(function() {

         });

        AssistAgentSDK.setConnectionLostCallback(function() {
        
        });

        AssistAgentSDK.setConnectionReestablishedCallback(function() {
            
        });

        AssistAgentSDK.setConnectionRetryCallback(function(retryCount, retryTimeInMilliSeconds) {
            
        });
      
        AssistAgentSDK.setConsumerEndedSupportCallback(function() {
            
        });

        AssistAgentSDK.setScreenShareRejectedCallback(function() {
            
        });

        AssistAgentSDK.setConsumerLeftCallback(function() {
            var canvasContainer = document.getElementById("share");
            var canvas = (canvasContainer.getElementsByTagName("canvas").length > 0) ? canvasContainer.getElementsByTagName("canvas")[0] : undefined;
            if (canvas) {
                var context = canvas.getContext("2d");
                context.clearRect(0, 0, canvas.width, canvas.height);
            }
        });

        AssistAgentSDK.setConsumerJoinedCallback(function() {

        });

        AssistAgentSDK.setCallEndedCallback(function() {
           
        });

    }

    function linkUi() {
        CallManager.setRemoteVideoElement(remote);
        CallManager.setLocalVideoElement(local);
    }

    // Initialise
    function init(sessionId) {
        var config = {
            sessionToken: sessionId,
            autoanswer: false,
            username: 'agent1',
            password: 'none',
            agentName: 'Blah',
            url: 'http://192.168.4.61:8080'
        };

        CallManager.init(config);
    }

}(sessionId);